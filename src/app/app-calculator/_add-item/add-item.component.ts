import { Component, Output, EventEmitter, Input, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Budget } from 'src/app/app.interface.ts';


@Component({
	selector: 'app-add-item',
	templateUrl: './add-item.component.html',
	styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

	addForm: any; // FormGroup | undefined;
	
	@Input() item: Budget | undefined;
	@Output() addItem = new EventEmitter<Budget>();

	isThisNewItem = true;

	constructor(private fb: FormBuilder) {	}

	ngOnInit() {
		if(!this.item) {
			this.addForm = this.fb.group({
				amount: ['', [Validators.required]],
				description: ['', [Validators.required]]
			});
			this.isThisNewItem = true;
		} else {
			this.addForm = this.fb.group({
				amount: [this.item.amount, [Validators.required]],
				description: [this.item.description, [Validators.required]]
			});
			this.isThisNewItem = false;
		}
	}

	/**
	 * 
	 * @param formDirective The input, of this function, is a "must be" - so we can "formDirective.resetForm();" - to clean completely the form
	 */
	insertItem(formDirective: FormGroupDirective){
		console.log('NOVO item: ', this.addForm?.value);
		this.addItem.emit(this.addForm?.value);
		this.isThisNewItem = true;

		formDirective.resetForm();

		this.addForm.reset();
	}

}
