import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Budget } from 'src/app/app.interface.ts';

@Component({
	selector: 'app-modif-item',
	templateUrl: './modif-item.component.html',
	styleUrls: ['./modif-item.component.scss']
})
export class ModifItemComponent implements OnInit {
		
	// @Input() item: Budget | undefined;	// It's already declared/@Injected()!

	constructor(
		public dialogRef: MatDialogRef<ModifItemComponent>,
		@Inject(MAT_DIALOG_DATA) public item: Budget
	) {
		console.log('=================', item);
 	}

	ngOnInit() {
	}

	onClose(updatedItem: Budget | undefined) {
		this.dialogRef.close(updatedItem);
		console.log(updatedItem)
	}
}
