import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppMaterialModule } from '../app.material.module';

import { CalculatorComponent } from '../app-calculator/calculator/calculator.component';

import { AddItemComponent } from './_add-item/add-item.component';
import { ModifItemComponent } from './_modif-item/modif-item.component';

import { BudgetItemListComponent } from './budget-item-list/budget-item-list.component';
import { BudgetItemComponent } from './budget-item-list/budget-item/budget-item.component';

import { FormatNumberPipe } from './pipe/format-number.pipe';

@NgModule({
	declarations: [
		CalculatorComponent,
		AddItemComponent,
		ModifItemComponent,
		BudgetItemListComponent,
		BudgetItemComponent
	],
	imports: [
		CommonModule,
		AppMaterialModule,
		FormsModule, ReactiveFormsModule
	],
	providers: [FormatNumberPipe],
})
export class AppCalculatorModule { }
