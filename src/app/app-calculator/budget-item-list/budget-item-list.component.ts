import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Budget } from 'src/app/app.interface.ts';

import { FormatNumberPipe } from '../pipe/format-number.pipe';
import { ModifItemComponent } from '../_modif-item/modif-item.component';

@Component({
	selector: 'app-budget-item-list',
	templateUrl: './budget-item-list.component.html',
	styleUrls: ['./budget-item-list.component.scss']
})
export class BudgetItemListComponent implements OnInit, OnChanges {

	itemsArray: Budget[] = [];
	@Input() item: Budget | undefined;
	@Output() displayEdit = new EventEmitter<number>();

	incomeTotal = 0;
	expensesTotal = 0;

	constructor(
		public readonly formatNumber: FormatNumberPipe,
		public dialog: MatDialog,
	) {	}

	ngOnInit() {
	}

	ngOnChanges() {
		if(this.item) {
			this.itemsArray.push(this.item);
			console.log('Array?:', this.itemsArray);

			if(this.item.amount > 0) { this.incomeTotal += this.item.amount; }
			if(this.item.amount < 0) { this.expensesTotal -= this.item.amount; }		
		}
	}

	doModifiedItem(item: Budget) {

		const dialogConfig = new MatDialogConfig();
		dialogConfig.data = item;
		console.log(item);
		dialogConfig.disableClose = true;
		dialogConfig.id = 'app-modif-item';
		dialogConfig.width = '900px';

		// show the edit modal
		const dialogRef = this.dialog.open(ModifItemComponent, dialogConfig);
		
		dialogRef.afterClosed().subscribe((result: Budget) => {
			console.log(result);
			// Change the old by the nem value:
			const index = this.itemsArray.indexOf(item);
			this.itemsArray[index] = result;
			
			// Problem is on AMOUNT part: we SUBtract what was in, and ADD the new item:
			if(item.amount > 0) { this.incomeTotal -= item.amount; }
			if(item.amount < 0) { this.expensesTotal += item.amount; }
			// And, finally, then ADD the 'result' part:
			if(result.amount > 0) { this.incomeTotal += result.amount; }
			if(result.amount < 0) { this.expensesTotal -= result.amount; }

			this.displayEdit.emit(this.incomeTotal - this.expensesTotal);
		})
	}

	doDeleteItem(item: any) {
		const msg = `Are you sure, you want to delete...?\n\n${JSON.stringify(item)}`;
		const warning = window.confirm(msg);
		if(warning) {
			// First, update the display:
			if(item.amount > 0) { this.incomeTotal -= item.amount; };
			if(item.amount < 0) { this.expensesTotal += item.amount; };
			// ... now delete it:
			const index = this.itemsArray.indexOf(item);
			this.itemsArray = this.itemsArray.filter((value: Budget, idx: number) => idx !== index);
			// Finally:
			this.displayEdit.emit(this.incomeTotal - this.expensesTotal)

		} /* else {
			window.document.addEventListener('click', item => {
				if(item.detail === 1) {
					item.stopImmediatePropagation();
					console.log(item);
				} else if(item.detail === 2) {
					console.log('2-dddddddddddddddddd')
				}
			})
		}*/
	}
}
