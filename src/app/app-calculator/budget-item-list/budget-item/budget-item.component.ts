import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Budget, Color } from 'src/app/app.interface.ts';

import { FormatNumberPipe } from '../../pipe/format-number.pipe';

@Component({
	selector: 'app-budget-item',
	templateUrl: './budget-item.component.html',
	styleUrls: ['./budget-item.component.scss']
})
export class BudgetItemComponent implements OnInit {

	nFormated: number | undefined;
	@Input() item: Budget | undefined;
	
	@Input() color: any; // Color | undefined; // 'red' | 'green' | undefined;

	@Output() modifiedItem = new EventEmitter<Budget>();
	@Output() deleteItem = new EventEmitter<Budget>();

	constructor(
		public readonly formatNumber: FormatNumberPipe,
		public dialog: MatDialog,
	) {}

	ngOnInit() {
		// this.nFormated = this.formatNumber.transform(this.item?.amount, 'pt-PT');
	}

	modifiedBudget(item: Budget) {
		this.modifiedItem.emit(item);
	}

	deleteBudget(item: Budget) {
		this.deleteItem.emit(item);
	}
}
