import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Budget } from 'src/app/app.interface.ts';

import { FormatNumberPipe } from '../pipe/format-number.pipe';
import { ModifItemComponent } from '../_modif-item/modif-item.component';

@Component({
	selector: 'app-calculator',
	templateUrl: './calculator.component.html',
	styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {

	number: number = 0; // -2000000;
	
	budgetItem: Budget | undefined;
	item: Budget | undefined

	constructor(
		public readonly formatNumber: FormatNumberPipe,
		public dialog: MatDialog,
	) {}

	ngOnInit() {

	}

	onReceiving(itemToAdd: Budget) {
		this.budgetItem = itemToAdd;
		this.number += this.budgetItem.amount;
	}

	onReceivingEdit(editedNumber: number) {
		this.number = editedNumber;
	}
}
