import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'formatNumber'
})
export class FormatNumberPipe implements PipeTransform {
	transform(value: any, selectedCulture?: any): any {
		if (value === 0) {
			return 0
		}
		if (!value) {
			return;
		}

		const userLang = navigator.language;  // Default - 'en-US'
		const formatConfig = {
			// style: "currency",
			// currency: "EUR", // CNY for Chinese Yen, EUR for Euro, ...
			// currencyDisplay: "symbol",
			// We don't need this...
			minimumFractionDigits: 2,
		};
		  
		if (selectedCulture) {
			return new Intl.NumberFormat(selectedCulture, formatConfig).format(value);
		} else {
			return new Intl.NumberFormat(userLang, formatConfig).format(value);
		}
	}

}
