export interface Budget {
    amount: number;
    description: string;
}

export type Color = 'red' | 'green'