import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppCalculatorModule } from './app-calculator/app-calculator.module';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ModifItemComponent } from './app-calculator/_modif-item/modif-item.component';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    
    AppCalculatorModule,

    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
